document.addEventListener("DOMContentLoaded", function() {

	console.log("Started...");
	$(".slider").slick({
		autoplay: true,
		autoplaySpeed: 1000,
		dots: true,
		arrows: false,
		infinite: true,
		slidesToShow: 3,
		speed: 650
	  });


});
